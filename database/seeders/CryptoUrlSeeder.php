<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class CryptoUrlSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \DB::table('settings')->insert([
            [
                'key' => 'crypto_url',
                'value' => 'https://api.etherscan.io/'
            ]
        ]);
    }
}
